function ItemView (el) {
    this.render = function (item) {
        //creating template of product 
        var infoBoxTpl = _.template('<div class="item-box">' +
                            '<div id="itemImg" class="item-img">' +
                                '<img src=<%= img %>>' +
                            '</div>' +
                            '<div class="item-header">' +
                                '<h2>' +
                                    '<%= title %>' +
                                '</h2>' +
                            '</div>' +
                            '<div class="item-body">' +
                                '<p class="description">' +
                                    '<%= description%>' +
                                '</p>' +
                                '<p class="note">' +
                                    '<%= note %>' +
                                '</p>' +
                            '</div>' +
                        '</div>');

        el.innerHTML = infoBoxTpl(item);
    };

    return this;
}