function Animator () {
    this.fadeIn = function (el, speed) {
        var opacity = 0,
            timerId;

        el.style.opacity = 0;
        el.style.filter = 'alpha(opacity=0)';
        
        timerId = setInterval(function () {
            opacity += 50 / speed;

            if (opacity >= 1) {
                clearInterval(timerId);
                opacity = 1;
                el.style.display = 'block';
                el.style.visibility = 'visible';
            }

            el.style.opacity = opacity;
            el.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
            el.style.display = 'block';
    		el.style.visibility = 'visible';
        }, 50);
    };

    this.fadeOut = function (el, speed) {
        var opacity = 1,
            timerId;
        
        timerId = setInterval(function () {
            opacity -= 50 / speed;

            if (opacity <= 0) {
                clearInterval(timerId);
                opacity = 0;
                el.style.display = 'none';
                el.style.visibility = 'hidden';
            }

            el.style.opacity = opacity;
            el.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
        }, 50);
    };

    this.reduceSize = function (el) {
    	var height = 200,
            timerId; 

    	timerId = setInterval(function () {
            height -= 20;

            if (height <= 0) {
                clearInterval(timerId);
                height = 0;
                el.style.display = 'none';
                el.style.visibility = 'hidden';
            }

            el.style.height = height + 'px';
        }, 50);
    };

    this.increaseSize = function (el) {
        var height = 0,
            timerId;
        
        timerId = setInterval(function () {
            height += 20;

            if (height >= 200) {
                clearInterval(timerId);
                height = 200;
                el.style.display = 'block';
               	el.style.visibility = 'visible';
            }

            el.style.height = height + 'px';
        }, 50);
    };

    return this;
}