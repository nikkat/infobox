window.onload = function () {
    function Main () {
        var itemBox = document.getElementById('itemBox'),
            showDetails = document.getElementById('showDetails'),
            hideDetails = document.getElementById('hideDetails'),
            itemView = new ItemView(itemBox),
            animator = new Animator(),
            currentStep = 0,

            items = [
                {
                    "title": "Time to Share: 6 for $3.99*",
                    "img": "img/comp_plate_promo1.png",
                    "description": "Lorem ipsum dolor sit amet. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut la bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exefcitalion ullamoo laboris nisi ut aliquip ex ea commodo oonsequat.",
                    "note": "* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.",
                    "productUrl": "/products/promo1.html"
                },
                {
                    "title": "Rise 'n shine",
                    "img": "img/comp_plate_promo2.png",
                    "description": "Lorem ipsum dolor sit amet. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut la bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exefcitalion ullamoo laboris nisi ut aliquip ex ea commodo oonsequat.",
                    "note": "* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.",
                    "productUrl": "/products/promo2.html"
                },
                {
                    "title": "PM Snackers: Brownie Bites",
                    "img": "img/comp_plate_promo3.png",
                    "description": "Lorem ipsum dolor sit amet. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut la bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exefcitalion ullamoo laboris nisi ut aliquip ex ea commodo oonsequat.",
                    "note": "* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.",
                    "productUrl": "/products/promo3.html"
                },
                {
                    "title": "PM Snackers: Brownie Bites new",
                    "img": "img/comp_plate_promo4.png",
                    "description": "Lorem ipsum dolor sit amet. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut la bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exefcitalion ullamoo laboris nisi ut aliquip ex ea commodo oonsequat.",
                    "note": "* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga. * At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.\n* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.\n* At vero eos et accusamus et iusto odo dtgntsslmos duclmus qui blandltlis praesentlum voluptatum delenrtl atque corruptl quos doQres et quas molestlas exceptun sint occaecatl cupidrtate non pro v dent, slmllique sunt In culpa qui otflcia deserunt mollrtia anlmi. id est la bo aim et dolorum tuga.",
                    "productUrl": "/products/promo4.html"
                }
            ];

        this.init = function () {
            //adding handlers for buttons Next and Prev
            if (document.addEventListener) {
                document.getElementById('next').addEventListener('click', _.bind(this.showNext, this), false);
                document.getElementById('prev').addEventListener('click', _.bind(this.showPrev, this), false);

                showDetails.addEventListener('click', _.bind(this.showInfo, this), false);
                hideDetails.addEventListener('click', _.bind(this.hideInfo, this), false);
            } else {
                document.getElementById('next').attachEvent('onclick', _.bind(this.showNext, this));
                document.getElementById('prev').attachEvent('onclick', _.bind(this.showPrev, this));

                showDetails.attachEvent('onclick', _.bind(this.showInfo, this));
                hideDetails.attachEvent('onclick', _.bind(this.hideInfo, this));
            }

            itemView.render(items[currentStep]);
        };

        this.showNext = function () {
            if (currentStep === (items.length - 1)) {
                currentStep = 0;
            } else {
                currentStep ++;
            }

            animator.fadeOut(document.getElementById('itemImg'), 500);
            itemView.render(items[currentStep]);
            animator.fadeIn(document.getElementById('itemImg'), 500);
        };

        this.showPrev = function () {
            if (currentStep === 0) {
                currentStep = items.length - 1;
            } else {
                currentStep --;
            }

            animator.fadeOut(document.getElementById('itemImg'), 500);
            itemView.render(items[currentStep]);
            animator.fadeIn(document.getElementById('itemImg'), 500);
        };

        this.showInfo = function () {
            showDetails.style.display = 'none';
            hideDetails.style.display = 'block';

            animator.fadeOut(document.getElementById('itemImg'), 500);
            animator.reduceSize(document.getElementById('itemImg'));
        };

        this.hideInfo = function () {
            hideDetails.style.display = 'none';
            showDetails.style.display = 'block';
            
            animator.increaseSize(document.getElementById('itemImg'));
            animator.fadeIn(document.getElementById('itemImg'), 500);
        };

        return this;   
    }

    var main = new Main();

    main.init();
}

